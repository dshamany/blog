![Creating Markdown Blogger](https://gitlab.com/dshamany/blog/-/raw/main/images/Creating_Markdown_Blogger.png?ref_type=heads&inline=false)

# Creating Markdown Blogger

Surely you've been in a situation where you feel you need a project to help you clear your mind. For me that was Markdown Blogger. It doesn't even attempt to be creative, yet in many ways the simplicity of it is beautiful.

## All You Need is Markdown

Create a file that contains links to your posts as demonstrated below:

```markdown
- [image title](https://<image-url>) | [post title](https://<post-url>)
```

You could also emit the image by leaving the content empty; however, they are required to properly parse a placeholder image should you not have one.

```markdown
- []() | [post title](https://<post-url>)
```

## Why Markdown?

Markdown is an easy tool to learn, easy to use format. Additionally it removes all distractions from the author. I'm currently writing this post in the [Helix Editor](https://helix-editor.com/) in my [Black Box](https://flathub.org/apps/com.raggesilver.BlackBox) terminal on [Fedora Silverblue](https://fedoraproject.org/silverblue/). 

## Benefits of Markdown

The simplicity of markdown allows the author to focus on the content and not the formatting. Many other apps attempt to accomplish that; however, markdown is truly the best. So much so that the best note taking app I know of uses markdown. [Obsidian](https://obsidian.md) is my go to note taking app, which I sync with my phone. 

## Version Control

Markdown is used in [Gitlab](https://gitlab.com) and [Github](https://github). This also means we could utilize Git (or any other version control system) to maintain our blog without a database. The benefits of version control are far too many to mention here, but one that stands out is the ability to track changes.

## Technologies

I focused on a minimal stack to reduce overhead and improve my skills quickly and deeply.  My belief is that constraints make us better engineers as it forces us to be creative with our work. Some of the best work I've seen occurred when the creator eliminated all the fancy tools and focused on a single task. That was my objective here.
#### Helix

[Helix](https://helix-editor.com) is my editor of choice. It's far easier to customize than the alternatives. Helix allows you to add as many [LSPs (Language-Server-Protocol)](https://en.wikipedia.org/wiki/Language_Server_Protocol) as you want.

Here are some of the benefits of Helix

- Code completion is a breeze
- Fuzzy file finder is quick to get used to (and prefer)
- Themes are easy to change

Some drawbacks: Folding code is not as easy as it is in Vim.
#### Rust

These days, Rust reins supreme as a language of choice for many engineers. Having dabbled with it earlier this year, it quickly grew on me. What sold me on it was that my code quality was much better. 

Some notable improvements include, but are not limited to the following:

- Immutable variables that you can read about [here](https://doc.rust-lang.org/book/ch03-01-variables-and-mutability.html)
- Error handling is excellent with the use of [`Option<T>`](https://doc.rust-lang.org/std/option/index.html) and [`Result<T, E>`](https://doc.rust-lang.org/std/result/) enums
- [Cargo](https://doc.rust-lang.org/book/ch01-03-hello-cargo.html), well featured package manager

For a relatively new language, the resources are more than enough for any developer or engineer to learn on their own.

#### Actix-Web

When I started writing Rust code, I used [Rocket](https://rocket.rs/) as a framework initially. While I liked it a lot and it was easy to learn, the way [Actix-Web](https://actix.rs) handles middleware convinced me to switch. Having written plenty of [Node.js](https://nodejs.org) applications early on in my career, it was easy to learn another framework as I knew what I was looking for.

#### Server-Side Rendering (SSR)

A client can be written in any framework given that there are RESTful APIs on the back end. To keep the projects I build in one place and using the most minimal tools possible, I elected to render my HTML in [Handlebars.js](https://handlebarsjs.com) and further utilize [HTMX](https://htmx.org) to allow for rendering partial views. 

One major benefit of SSR is that it allows the application to remain up-to-date without redeployment on the clients device. You, the user, will always use the latest update with a single page refresh.

#### OCI Container (Docker)

While most people use [Docker](https://docker.com), this project uses [Podman](https://podman.io). Podman is included in Fedora Silverblue and allows multiple containers to run in a pod. You can then use an initialized pod and generate a [Kubernetes](https://kubernetes.io) manifest to run your entire application anywhere Kubernetes is accepted.
#### Deployment

Deployment was made easy because of [Fly.io](https://fly.io) which utilizes a CLI tool (flyctl) to deploy a dockerized project directly to the platform.
## Future Plans

I'm planning on expanding the Blog Platform to allow for customization. I have some ideas, yet nothing is finalized. For instance, allowing custom templates with the request would be nice. After all I've accounted for it by creating a folder structure as suck:

	- Root
		- Markdowns
		- Images
		- Templates

## The Goal

Create a blog that doesn't require too much boilerplate to run and customize.


## Links

#### Markdown Blogger
[Source](https://gitlab.com/dshamany/markdown-blogger) | [Web App](https://markdown-blogger.fly.dev)
